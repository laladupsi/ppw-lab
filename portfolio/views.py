from django.shortcuts import render
from django.http import HttpResponse
from .forms import JadwalPribadi
from .models import Jadwal

response = {}
def index(request):
    form = JadwalPribadi()
    response['form'] = form
    hasil = Jadwal.objects.all()
    response['result']=hasil
    return render(request, 'jadwal.html', response)

def facts(request):
    return render(request,'facts.html')

def message_post(request):
    form = JadwalPribadi(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['day'] = request.POST['day']
        response['place'] = request.POST['place']
        response['category'] = request.POST['category']
        response['date'] = request.POST['date']
        message = JadwalPribadi(name=response['name'],day=response['day'],place=response['place'],
            category=response['category'], date=response['date'])
        message.save()

        html = 'jadwal.html'
        return render(request,html,response)
    else:
        return render(request,'jadwal.html',response)

def delete_recs(request):
    Jadwal.objects.all().delete()
    context= {}
    return render(request, 'jadwal.html', context)
# def show_result(request):
#     result = Jadwal.objects.all()
#     response['result']=result
#     return render(request,'jadwal.html', response)

