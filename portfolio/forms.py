from django import forms


class JadwalPribadi(forms.Form):
    attrs={'class':'form-control',}
    attrs_time = {'class':'form-control','type':'date'}
    name = forms.CharField(label='Nama', required=True,
        max_length=50,empty_value='Anonymous',)
        #widget=forms.TextInput(attrs=attrs))
    place = forms.CharField(label='Place', required=True,
        max_length=30,empty_value='Anonymous',)
        #widget=forms.TextInput(attrs=attrs))
    category = forms.CharField(label='Category', required=True,
        max_length=30,empty_value='Anonymous',)
        #widget=forms.TextInput(attrs=attrs))
    date = forms.DateTimeField(label='Date, time', required=True,
        widget=forms.DateInput(attrs=attrs_time))