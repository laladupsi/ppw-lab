from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Jadwal(models.Model):
    name = models.CharField(max_length=50)
    place = models.CharField(max_length=30)
    category = models.CharField(max_length=30)
    date = models.DateField()
    time = models.DateTimeField(default=timezone.now)