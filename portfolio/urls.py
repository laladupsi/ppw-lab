from django.urls import include, path
from django.contrib import admin
from .views import *

app_name = 'portfolio'

urlpatterns = [
    path('', index),
    path('jadwalku/', message_post, name='message_post'),
    path('jadwalku/del',delete_recs,name='delete_recs')

]
